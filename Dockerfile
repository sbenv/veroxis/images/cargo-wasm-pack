FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY "wasm-pack-bin" "/usr/local/bin/wasm-pack"
